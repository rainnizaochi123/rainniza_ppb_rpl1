import 'dart:io';

void main(List<String> args) {
  print("Apakah Ingin Menginstall Aplikasi ?");

  String? pilihan = stdin.readLineSync()!;
  (pilihan == 'Y')
    ? print("Instalasi Berjalan")
    : print("Instalasi Dibatalkan");
}
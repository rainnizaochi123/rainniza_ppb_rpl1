import 'dart:io';

void main(List<String> args) {
  var nilaiMax = 100;
  print("Masukkan Nilai Anda : ");
  String? nilai = stdin.readLineSync()!;
  
  if (int.parse(nilai) > nilaiMax) {
    print("Nilai Maksimal adalah 100 !");
} else if (int.parse(nilai) >= 80) {
  print("Grade anda A");
} else if (int.parse(nilai) >= 70) {
  print("Grade anda B");
} else if (int.parse(nilai) >= 60) {
  print("Grade anda C");
} else {
  print("Anda Tidak Lulus");
 }
}


import 'dart:io';

void main(List<String> args) {

  print("Nama Depan :");
  String? inputNamaDepan = stdin.readLineSync()!;
  print("Nama Depan: ${inputNamaDepan}");

  print("Nama tengah :");
  String? inputNamaTengah = stdin.readLineSync()!;
  print("Nama tengah : ${inputNamaTengah}");

  print("Nama belakang :");
  String? inputNamaBelakang = stdin.readLineSync()!;
  print("Nama belakang : ${inputNamaBelakang}");

  late String namaLengkap =
    "${inputNamaDepan} ${inputNamaTengah} ${inputNamaBelakang}";
  print("Nama lengkap anda adalah: ");
  print(namaLengkap);
  
}